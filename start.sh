#!/bin/bash

sudo chown -R $USER *
sudo chgrp -R $USER *
docker-compose up
docker-compose down
